/*package com.chatroom.mockhack.chatroom;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;

import com.chatroom.Dao.ChatRoomDAO;
import com.chatroom.model.ChatroomDetails;
import com.chatroom.service.ChatroomService;
import com.chatroom.serviceImpl.ChatroomServiceImpl;

public class ChatRoomServiceTest {

	@InjectMocks
	private ChatroomService chatroomService;

	@Mock
    private ChatroomDetails chatroomDetails;
	@Mock
    private ChatRoomDAO chatRoomDAO;

	@Before
	    public void setUp() throws Exception {
	        MockitoAnnotations.initMocks(this);
	    }

	@Test
	public void createChatRoomTest() {
//		ChatroomDetails chatroomDetails = new ChatroomDetails();
		chatroomDetails.setChatroomName("TestChatRoom");
		chatroomDetails.setMaxNoOfUser("120");
		chatroomDetails.setUserId("110");		
		ChatroomServiceImpl cs= new ChatroomServiceImpl();
		
		Mockito.when(chatRoomDAO.createChatRoom(chatroomDetails)).thenReturn(chatroomDetails);
		cs.createChatRoom(chatroomDetails);
	}

}
*/