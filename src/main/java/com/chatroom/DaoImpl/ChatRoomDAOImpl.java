package com.chatroom.DaoImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.chatroom.Dao.ChatRoomDAO;
import com.chatroom.model.ChatroomDetails;
import com.chatroom.model.UserDetails;
import com.chatroom.model.UserMessages;
/**
 * 
 * 
 * @author dilipkm
 *
 */
@Repository
public class ChatRoomDAOImpl implements ChatRoomDAO {
	@Autowired
	JdbcTemplate jdbcTemplate;
	@Autowired
	DataSource dataSource;
	
	private static final String getUser = "SELECT userId, userName, gender, phoneNumber FROM UserDetails where userId=?";
	
	@Override
	public UserDetails createUser(UserDetails userDetails) {
		SimpleJdbcInsert insertObj = new SimpleJdbcInsert(jdbcTemplate).withTableName("UserDetails");		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId", userDetails.getUserId());	
		map.put("userName", userDetails.getUserName());	
		map.put("gender", userDetails.getGender());
		map.put("phoneNumber", userDetails.getPhoneNumber());
		insertObj.execute(map);		
		return userDetails;
		
	}
	@Override
	public List<Map<String, Object>> getUser(String userID) {
		return jdbcTemplate.queryForList(getUser, userID);
	}
	
	@Override
	public UserMessages saveChatMessage(UserMessages userMessages) {
		SimpleJdbcInsert insertObj = new SimpleJdbcInsert(jdbcTemplate).withTableName("UserMessage");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId", userMessages.getUserId());
		map.put("message", userMessages.getMessage());
		insertObj.execute(map);
		return userMessages;
	}
	@Override
	public ChatroomDetails createChatRoom(ChatroomDetails chatroomDetails) {
		SimpleJdbcInsert insertObj = new SimpleJdbcInsert(jdbcTemplate).withTableName("charRoomDetails");
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("userId", chatroomDetails.getUserId());
		map.put("chatroomName", chatroomDetails.getChatroomName());
		map.put("maxNoOfUser", chatroomDetails.getMaxNoOfUser());
		insertObj.execute(map);
		return chatroomDetails;
	}

}

