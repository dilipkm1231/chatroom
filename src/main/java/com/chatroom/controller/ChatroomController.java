package com.chatroom.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.chatroom.model.ChatroomDetails;
import com.chatroom.model.UserDetails;
import com.chatroom.model.UserMessages;
import com.chatroom.service.ChatroomService;

@RestController
@RequestMapping("/chatroomApi")
public class ChatroomController {

	@Autowired
	ChatroomService chatroomService;
	private static final Logger logger = LoggerFactory.getLogger(ChatroomController.class);
	
	@RequestMapping(value = "/createUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDetails> createOffer(@Valid @RequestBody UserDetails userDetails) {
		logger.debug("Executing createOffer");
		
		chatroomService.createUser(userDetails);
		logger.debug("createOffer Successfull");
		return new ResponseEntity<>(userDetails, HttpStatus.CREATED);
	}
	
	@RequestMapping( value= "/getUserDetails/{userId}", method= RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> getUsers(@PathVariable("userId")  String userID){
		logger.debug("Executing getUsers");
		chatroomService.getUser(userID);
		logger.debug("getUsers Completed");
		return null;
	}
	
	@RequestMapping(value = "/chatMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserMessages> chatMessage(@Valid @RequestBody UserMessages userMessages) {
		logger.debug("Executing chatMessage");
		chatroomService.saveChatMessage(userMessages);
		logger.debug("chatMessage Completed");
		return new ResponseEntity<>(userMessages, HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/createChatRoom", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ChatroomDetails> createChatRoom(@Valid @RequestBody ChatroomDetails chatroomDetails) {
		logger.debug("Executing createChatRoom");
		chatroomService.createChatRoom(chatroomDetails);
		logger.debug("createChatRoom Completed");
		return new ResponseEntity<>(chatroomDetails, HttpStatus.CREATED);
	}
	
	
	
}
