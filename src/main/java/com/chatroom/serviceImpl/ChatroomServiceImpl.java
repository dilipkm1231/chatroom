package com.chatroom.serviceImpl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatroom.Dao.ChatRoomDAO;
import com.chatroom.model.ChatroomDetails;
import com.chatroom.model.UserDetails;
import com.chatroom.model.UserMessages;
import com.chatroom.service.ChatroomService;

@Service
public class ChatroomServiceImpl implements ChatroomService {

	@Autowired
	ChatRoomDAO chatRoomDAO; 
	@Override
	public UserDetails createUser(UserDetails userDetails) {
		
		chatRoomDAO.createUser(userDetails);
		
		return null;
	}
	@Override
	public List<Map<String, Object>> getUser(String userID) {
		return chatRoomDAO.getUser(userID);
	}
	@Override
	public UserMessages saveChatMessage(UserMessages userMessages) {
		chatRoomDAO.saveChatMessage(userMessages);
		return chatRoomDAO.saveChatMessage(userMessages);
	}
	@Override
	public ChatroomDetails createChatRoom(ChatroomDetails chatroomDetails) {
		
		
		return chatRoomDAO.createChatRoom(chatroomDetails);
	}
	

}
