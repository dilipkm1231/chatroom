package com.chatroom;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.chatroom.*"})
public class ChatroomApplication {
//added again
	public static void main(String[] args) {
		SpringApplication.run(ChatroomApplication.class, args);
	}
}
