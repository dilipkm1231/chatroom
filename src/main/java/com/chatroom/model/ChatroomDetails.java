package com.chatroom.model;

public class ChatroomDetails {

    String userId;
    String chatroomName;
    String maxNoOfUser;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getChatroomName() {
		return chatroomName;
	}
	public void setChatroomName(String chatroomName) {
		this.chatroomName = chatroomName;
	}
	public String getMaxNoOfUser() {
		return maxNoOfUser;
	}
	public void setMaxNoOfUser(String maxNoOfUser) {
		this.maxNoOfUser = maxNoOfUser;
	}
    
    
}
