package com.chatroom.Dao;

import java.util.List;
import java.util.Map;

import com.chatroom.model.ChatroomDetails;
import com.chatroom.model.UserDetails;
import com.chatroom.model.UserMessages;

public interface ChatRoomDAO {
	
	public UserDetails createUser(UserDetails userDetails);
	public UserMessages saveChatMessage(UserMessages  userMessages);
	public ChatroomDetails createChatRoom(ChatroomDetails chatroomDetails);
	public List<Map<String, Object>> getUser(String userID);
}
